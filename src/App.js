import { Route, Switch } from 'react-router-dom'

import { privateRouter } from './routers/routers'

import './App.css';
import Footer from './components/Footer/Footer';



function App() {
  return (
    <>
    <Switch>
    {privateRouter.map(({path, Components}) => <Route path={path} exact component={Components} />)}
  </Switch>
  <Footer />
  </>
  );
}

export default App;
