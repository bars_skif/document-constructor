import { useState } from 'react'

import { Button, FormControl, Grid, IconButton, InputAdornment, InputLabel, OutlinedInput, TextField } from '@material-ui/core';
import { Visibility, VisibilityOff } from '@material-ui/icons';
import cl from './LoginPage.module.css'

import logo from '../../sourse/logo.png'


const LoginPage = () => {
    const [values, setValues] = useState({
        amount: '',
        password: '',
        weight: '',
        weightRange: '',
        showPassword: false,
      });


      const handleChange = (prop) => (event) => {
        setValues({ ...values, password : event.target.value });
       
      };

      const handleClickShowPassword = () => {
        setValues({ ...values, showPassword: !values.showPassword });
      };


    return (
        <div className={cl.wrap}>


            <Grid
                container
                direction="column"
                justifyContent="center"
                alignItems="center"
                className={cl.form_login}
            >
                          

    <div className={cl.logo_grup}><img src={logo} alt="logo"/><span className={cl.logo_two}> .doc</span></div>
                <Grid
                    container
                    direction="column"
                    //  justifyContent="center"
                    alignItems="center"
                    className={cl.form_login_item}
                >
                    <div className={cl.form_login_title}>войти в систему</div>
                    <Grid
                        container
                        direction="column"
                        justifyContent="center"
                        style={{ width: "90%", marginTop: '35px', marginBottom: '50px', height: "100px" }}
                    >
                        <form className={cl.form}>
                            <TextField
                                size="small"
                                variant='outlined'
                                margin='normal'
                                required
                                fullWidth
                                id='email'
                                label='Email'
                                name='email'
                                autoComplete='email'
                                autoFocus
                            // onChange={e => setEmail(e.target.value)}
                            />
                            <FormControl className={`${cl.margin} ${cl.textField}`} variant="outlined" size="small">
                                <InputLabel htmlFor="outlined-adornment-password">Password *</InputLabel>
                                <OutlinedInput
                                    className={cl.input_passw}
                                    id="outlined-adornment-password"
                                    type={!values.showPassword ? 'text' : 'password'}
                                    value={values.password}
                                    onChange={handleChange('password')}
                                    endAdornment={
                                        <InputAdornment position="end">
                                            <IconButton
                                                aria-label="toggle password visibility"
                                                onClick={handleClickShowPassword}
                                                onMouseDown={(e) => console.log('chenge =>>', e.target.value)}
                                                edge="end"
                                            >
                                                {values.showPassword ? <Visibility /> : <VisibilityOff />}
                                            </IconButton>
                                        </InputAdornment>
                                    }
                                    labelWidth={85}
                                />
                            </FormControl>
                            <Button
                                // type='submit'
                                // fullWidth
                                variant='contained'
                                // color='primary'
                                style={{ backgroundColor: '#676767' }}
                                className={cl.submit}

                            >
                                Войти
                            </Button>
                        </form>


                    </Grid>

                    <Grid container style={{ justifyContent: "space-around" }}>

                        <Grid item>
                            <hr className={cl.delimiter} />
                            <div className={cl.new_akk}>СОЗДАТЬ АККАУНТ</div>

                        </Grid>
                    </Grid>

                </Grid>



            </Grid>
        </div>
    )
}




export default LoginPage;